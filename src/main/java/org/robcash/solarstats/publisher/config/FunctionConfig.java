/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.publisher.config;

import java.util.function.Consumer;

import org.robcash.solarstats.publisher.functions.SolarStatsPublisher;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;

/**
 * Spring bean configuration for functions.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration
@EnableConfigurationProperties(FunctionConfigProperties.class)
public class FunctionConfig
{

	/**
	 * Create new instance of FunctionConfig.
	 */
	@Bean
	public Consumer<Message<byte[]>> publishSolarStats()
	{
		return new SolarStatsPublisher();
	}

}

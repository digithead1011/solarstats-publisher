/*
 * Copyright (C) 2020 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.publisher.config;

import org.robcash.solarstats.publisher.ifttt.MakerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * IFTTT Maker Spring configuration.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration
@EnableConfigurationProperties(MakerConfigProperties.class)
public class MakerConfig
{
	/** Configuration properties. */
	@Autowired
	private MakerConfigProperties config;

	@Bean
	public MakerService iftttMakerService()
	{
		return new MakerService(makerRestClient());
	}

	@Bean
	public WebClient makerRestClient()
	{
		return WebClient.builder()
				.baseUrl(this.config.getBaseUrl())
				.build();
	}
}

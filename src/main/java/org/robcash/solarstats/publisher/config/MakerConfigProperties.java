/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.publisher.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Configuration properties for the IFTTT Maker service
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@ConfigurationProperties("ifttt.maker")
public class MakerConfigProperties
{
	/** API Endpoint. */
	private String baseUrl;

	/** Event name. */
	private String eventName;

	/** Date format. */
	private String dateFormat;

	/**
	 * Create new instance of MakerConfigProperties.
	 */
	public MakerConfigProperties()
	{
		super();
	}

	/**
	 * Get the base URL.
	 *
	 * @return Base URL.
	 */
	public String getBaseUrl()
	{
		return this.baseUrl;
	}

	/**
	 * Set the base URL.
	 *
	 * @param argBaseUrl Base URL.
	 */
	public void setBaseUrl(final String argBaseUrl)
	{
		this.baseUrl = argBaseUrl;
	}

	/**
	 * Get the event name.
	 *
	 * @return Event name.
	 */
	public String getEventName()
	{
		return this.eventName;
	}

	/**
	 * Set the event name.
	 *
	 * @param argEventName Event name.
	 */
	public void setEventName(final String argEventName)
	{
		this.eventName = argEventName;
	}

	/**
	 * Get the date format.
	 *
	 * @return Date format.
	 */
	public String getDateFormat()
	{
		return this.dateFormat;
	}

	/**
	 * Set the date format.
	 *
	 * @param argDateFormat Date format.
	 */
	public void setDateFormat(final String argDateFormat)
	{
		this.dateFormat = argDateFormat;
	}

}

/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.publisher.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Configuration properties for the RetrieveStatisticsFunction.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@ConfigurationProperties(prefix = "solarstats.publisher")
public class FunctionConfigProperties
{

	/** Whether or not to dump the environment variables. */
	private boolean debugEnvironment = false;

	/**
	 * Create new instance of FunctionConfigProperties.
	 */
	public FunctionConfigProperties()
	{
		super();
	}

	/**
	 * Get the debugEnvironment flag.
	 *
	 * @return {@code true} if the environment should be logged.
	 */
	public boolean isDebugEnvironment()
	{
		return this.debugEnvironment;
	}

	/**
	 * Set the debugEnvironment flag.
	 *
	 * @param argDebugEnvironment Debug flag.
	 */
	public void setDebugEnvironment(final boolean argDebugEnvironment)
	{
		this.debugEnvironment = argDebugEnvironment;
	}

}

/*
 * Copyright (C) 2020-2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.publisher.ifttt;

/**
 * IFTTT Maker request.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class MakerRequest
{
	/** Value 1. */
	private String value1;

	/** Value 2. */
	private String value2;

	/** Value 3. */
	private String value3;

	/**
	 * Create new instance of MakerRequest.
	 */
	public MakerRequest()
	{
		super();
	}

	/**
	 * Get the value1.
	 *
	 * @return value1.
	 */
	public String getValue1()
	{
		return this.value1;
	}

	/**
	 * Set the value1.
	 *
	 * @param argValue1 value1.
	 */
	public void setValue1(final String argValue1)
	{
		this.value1 = argValue1;
	}

	/**
	 * Get the value2.
	 *
	 * @return value2.
	 */
	public String getValue2()
	{
		return this.value2;
	}

	/**
	 * Set the value2.
	 *
	 * @param argValue2 value2.
	 */
	public void setValue2(final String argValue2)
	{
		this.value2 = argValue2;
	}

	/**
	 * Get the value3.
	 *
	 * @return value3.
	 */
	public String getValue3()
	{
		return this.value3;
	}

	/**
	 * Set the value3.
	 *
	 * @param argValue3 value3.
	 */
	public void setValue3(final String argValue3)
	{
		this.value3 = argValue3;
	}

}

/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.publisher.ifttt;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.robcash.customer.model.Customer;
import org.robcash.solarstats.monitoring.model.SolarProductionData;
import org.robcash.solarstats.publisher.config.MakerConfigProperties;
import org.robcash.utils.tracing.annotation.Traced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import io.netty.handler.timeout.TimeoutException;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

/**
 * IFTTT Maker service.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class MakerService
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(MakerService.class);

	/** Maker request path. */
	private static final String REQUEST_PATH = "/trigger/{event}/json/with/key/{key}";

	/** Configuration. */
	@Autowired
	private MakerConfigProperties config;

	/** REST client. */
	private final WebClient restClient;

	/**
	 * Create new instance of Maker Service.
	 *
	 * @param argRestClient REST client.
	 */
	public MakerService(final WebClient argRestClient)
	{
		this.restClient = argRestClient;
	}

	/**
	 * Publish statistics to IFTTT for a Customer
	 *
	 * @param argData Solar data.
	 * @param argCustomer Customer.
	 */
	@Traced("Publish Stats to IFTTT")
	public Mono<Void> publishStats(final SolarProductionData argData, final Customer argCustomer)
	{
		final DateTimeFormatter parser = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

		final ZonedDateTime periodEnd = ZonedDateTime.from(parser.parse(argData.getPeriodEnd()));
		final String endDate = DateTimeFormatter.ofPattern(this.config.getDateFormat(), Locale.US).format(periodEnd);

		// Create data object to POST to IFTTT Maker
		final EnhancedMakerRequest makerData = new EnhancedMakerRequest()
				.dataSource(argData.getDataSource())
				.systemName(argData.getSystemName())
				.periodStart(argData.getPeriodStart())
				.periodEnd(argData.getPeriodEnd())
				.periodEndDate(endDate)
				.energyProduced(argData.getWattHours());

		// Publish to IFTTT
		LOG.info("Publishing production data for {} ({}) to IFTTT", argCustomer.getName(), argData.getSystemName());
		return this.restClient
				.post()
				.uri(REQUEST_PATH, this.config.getEventName(), argCustomer.getIftttSolarStatsMakerKey())
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(makerData)
				.exchangeToMono(r -> r.releaseBody())
				.retryWhen(
						Retry.backoff(2, Duration.ofMillis(100))
								.filter(t -> t instanceof TimeoutException))
				.doOnSuccess(v -> {
					LOG.info("Success!");
				})
				.doOnError(t -> {
					LOG.warn("Failed to send stats to IFTTT", t);
				});
	}
}

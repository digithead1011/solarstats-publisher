/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.publisher.ifttt;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class EnhancedMakerRequest
{
	/** Data source. */
	@JsonProperty
	private String dataSource;

	/** System name. */
	@JsonProperty
	private String systemName;

	/** Start of period for which energy is reported. */
	@JsonProperty
	private String periodStart;

	/** End of period for which energy is reported. */
	@JsonProperty
	private String periodEnd;

	/** Stats end date. */
	@JsonProperty
	private String periodEndDate;

	/** Energy produced. */
	@JsonProperty
	private Integer energyProduced;

	/**
	 * Create new instance of EnhancedMakerRequest.
	 */
	public EnhancedMakerRequest()
	{
		super();
	}

	/**
	 * Get the data source.
	 *
	 * @return Data source.
	 */
	public String getDataSource()
	{
		return this.dataSource;
	}

	/**
	 * Set the data source.
	 *
	 * @param argDataSource Data source.
	 */
	public void setDataSource(final String argDataSource)
	{
		this.dataSource = argDataSource;
	}

	/**
	 * Set the data source.
	 *
	 * @param argDataSource Data source.
	 * @return Request being modified (this).
	 */
	public EnhancedMakerRequest dataSource(final String argDataSource)
	{
		this.dataSource = argDataSource;
		return this;
	}

	/**
	 * Get the system name.
	 *
	 * @return System name.
	 */
	public String getSystemName()
	{
		return this.systemName;
	}

	/**
	 * Set the system name.
	 *
	 * @param argSystemName System name.
	 */
	public void setSystemName(final String argSystemName)
	{
		this.systemName = argSystemName;
	}

	/**
	 * Set the system name.
	 *
	 * @param argSystemName System name.
	 * @return Request being modified (this).
	 */
	public EnhancedMakerRequest systemName(final String argSystemName)
	{
		this.systemName = argSystemName;
		return this;
	}

	/**
	 * Get the period start.
	 *
	 * @return Period start.
	 */
	public String getPeriodStart()
	{
		return this.periodStart;
	}

	/**
	 * Set the period start.
	 *
	 * @param argPeriodStart Period start.
	 */
	public void setPeriodStart(final String argPeriodStart)
	{
		this.periodStart = argPeriodStart;
	}

	/**
	 * Set the period start.
	 *
	 * @param argPeriodStart Period start.
	 * @return Request being modified (this).
	 */
	public EnhancedMakerRequest periodStart(final String argPeriodStart)
	{
		this.periodStart = argPeriodStart;
		return this;
	}

	/**
	 * Get the period end.
	 *
	 * @return Period end.
	 */
	public String getPeriodEnd()
	{
		return this.periodEnd;
	}

	/**
	 * Set the period end.
	 *
	 * @param argPeriodEnd Period end.
	 */
	public void setPeriodEnd(final String argPeriodEnd)
	{
		this.periodEnd = argPeriodEnd;
	}

	/**
	 * Set the period end.
	 *
	 * @param argPeriodEnd Period end.
	 * @return Request being modified (this).
	 */
	public EnhancedMakerRequest periodEnd(final String argPeriodEnd)
	{
		this.periodEnd = argPeriodEnd;
		return this;
	}

	/**
	 * Get the period end date.
	 *
	 * @return Period end date.
	 */
	public String getPeriodEndDate()
	{
		return this.periodEndDate;
	}

	/**
	 * Set the period end date.
	 *
	 * @param argPeriodEndDate Period end date.
	 */
	public void setPeriodEndDate(final String argPeriodEndDate)
	{
		this.periodEndDate = argPeriodEndDate;
	}

	/**
	 * Set the period end date.
	 *
	 * @param argPeriodEndDate Period end date.
	 * @return Request being modified (this).
	 */
	public EnhancedMakerRequest periodEndDate(final String argPeriodEndDate)
	{
		this.periodEndDate = argPeriodEndDate;
		return this;
	}

	/**
	 * Get the energy produced.
	 *
	 * @return Energy produced.
	 */
	public Integer getEnergyProduced()
	{
		return this.energyProduced;
	}

	/**
	 * Set the energy produced.
	 *
	 * @param argEnergyProduced Energy produced.
	 */
	public void setEnergyProduced(final Integer argEnergyProduced)
	{
		this.energyProduced = argEnergyProduced;
	}

	/**
	 * Set the energy produced.
	 *
	 * @param argEnergyProduced Energy produced.
	 * @return Request being modified (this).
	 */
	public EnhancedMakerRequest energyProduced(final Integer argEnergyProduced)
	{
		this.energyProduced = argEnergyProduced;
		return this;
	}

}

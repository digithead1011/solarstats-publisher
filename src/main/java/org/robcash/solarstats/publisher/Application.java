/*
 * Copyright (C) 2020-2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.publisher;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Spring Boot application.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class Application
{

}

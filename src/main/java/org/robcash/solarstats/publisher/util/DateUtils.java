/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.publisher.util;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Date utilities.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public final class DateUtils
{

	/**
	 * Create new instance of DateUtils.
	 */
	private DateUtils()
	{
		// Prevent instantiation
		super();
	}

	public static String formatDateInTimezone(final Date argDate, final String argTimezone)
	{
		final ZoneId zone = ZoneId.of(argTimezone);
		final ZonedDateTime timestamp = ZonedDateTime.ofInstant(argDate.toInstant(), zone);
		return DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(timestamp);
	}
}

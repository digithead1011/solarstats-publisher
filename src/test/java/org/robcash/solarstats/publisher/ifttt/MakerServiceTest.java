/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.publisher.ifttt;

import java.io.IOException;
import java.util.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.robcash.customer.model.Customer;
import org.robcash.solarstats.monitoring.model.SolarProductionData;
import org.robcash.solarstats.publisher.config.MakerConfigProperties;
import org.robcash.solarstats.publisher.config.MakerServiceTestConfig;
import org.robcash.solarstats.publisher.web.client.utils.EnhancedQueueDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.reactive.function.client.WebClient;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

/**
 * Unit tests for {@link MakerService}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@SpringBootTest(classes = { MakerServiceTestConfig.class })
@ActiveProfiles("test")
public class MakerServiceTest
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(MakerServiceTest.class);

	/** Mock web server. */
	private static MockWebServer mockIftttService;

	/** Dispatcher. */
	private static EnhancedQueueDispatcher dispatcher;

	/** Application context. */
	@Autowired
	private ApplicationContext appContext;

	/** Configuration. */
	@Autowired
	private MakerConfigProperties config;

	/** Maker service being tested. */
	private MakerService maker;

	/** WebClient being used by the MakerService. */
	private WebClient testClient;

	@BeforeAll
	public static void setUp() throws IOException
	{
		mockIftttService = new MockWebServer();
		dispatcher = new EnhancedQueueDispatcher();
		mockIftttService.setDispatcher(dispatcher);
		mockIftttService.start();
	}

	@AfterAll
	public static void tearDown() throws IOException
	{
		mockIftttService.shutdown();
	}

	@BeforeEach
	public void initializeTest()
	{
		final int port = mockIftttService.getPort();
		this.testClient = WebClient.builder()
				.baseUrl(String.format("http://localhost:%d", port))
				.build();
		this.maker = (MakerService) this.appContext.getBean("makerTestService", this.testClient);
	}

	@Test
	public void testPublishStats()
	{
		// Mock server response from maker
		final MockResponse mockResponse = new MockResponse()
				.addHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.setResponseCode(200);
		mockIftttService.enqueue(mockResponse);

		final Date now = new Date();
		final int wattHours = 99999;
		final String systemId = "SYSTEM-1";
		final String periodStart = "2022-04-04T00:00:00-04:00";
		final String periodEnd = "2022-04-04T23:59:59-04:00";
		final SolarProductionData data = new SolarProductionData()
				.periodStart(periodStart)
				.periodEnd(periodEnd)
				.asOfDate(now)
				.systemId(systemId)
				.wattHours(wattHours);
		final Customer customer = new Customer().iftttSolarStatsMakerKey("makerKey");
		this.maker.publishStats(data, customer).block();

		// Make sure server was called.
		Assertions.assertEquals(1, mockIftttService.getRequestCount());
		final RecordedRequest lastRequest = dispatcher.getLastRequest();
		final String expectedPath = "/trigger/" + this.config.getEventName() + "/json/with/key/"
				+ customer.getIftttSolarStatsMakerKey();
		Assertions.assertEquals(expectedPath, lastRequest.getPath());
		Assertions.assertEquals(MediaType.APPLICATION_JSON_VALUE, lastRequest.getHeader(HttpHeaders.CONTENT_TYPE));
		LOG.info("Last request body: {}", dispatcher.getLastRequestBody());
	}
}

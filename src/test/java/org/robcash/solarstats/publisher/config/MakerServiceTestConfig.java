/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.publisher.config;

import org.robcash.solarstats.publisher.ifttt.MakerService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Configuration for {@link org.robcash.solarstats.publisher.ifttt.MakerServiceTest}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration
@EnableConfigurationProperties(MakerConfigProperties.class)
public class MakerServiceTestConfig
{

	@Bean
	@Lazy
	public MakerService makerTestService(final WebClient argRestClient)
	{
		return new MakerService(argRestClient);
	}

}

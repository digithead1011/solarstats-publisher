/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.publisher.web.client.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.QueueDispatcher;
import okhttp3.mockwebserver.RecordedRequest;

public class EnhancedQueueDispatcher extends QueueDispatcher
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(EnhancedQueueDispatcher.class);

	private final Stack<RecordedRequest> previousRequests;
	private String lastRequestBody;

	/**
	 * Create new instance of CountingDispatcher.
	 */
	public EnhancedQueueDispatcher()
	{
		super();
		this.previousRequests = new Stack<>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MockResponse dispatch(final RecordedRequest argRequest) throws InterruptedException
	{
		this.previousRequests.add(argRequest);

		final ByteArrayOutputStream os = new ByteArrayOutputStream();
		try
		{
			argRequest.getBody().copyTo(os);
			this.lastRequestBody = os.toString();
		}
		catch (final IOException ex)
		{
			LOG.error("Failed to copy request body", ex);
		}

		return super.dispatch(argRequest);
	}

	public RecordedRequest getLastRequest()
	{
		return this.previousRequests.pop();
	}

	public String getLastRequestBody()
	{
		return this.lastRequestBody;
	}

}

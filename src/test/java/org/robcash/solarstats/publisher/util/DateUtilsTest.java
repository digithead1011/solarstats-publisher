/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.publisher.util;

import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
class DateUtilsTest
{
	/**
	 * Test method for
	 * {@link org.robcash.solarstats.publisher.util.DateUtils#formatDateInTimezone(java.util.Date, java.lang.String)}.
	 */
	@Test
	void testFormatDateInTimezone()
	{
		Date d = new Date(1649044800000L);
		String s = DateUtils.formatDateInTimezone(d, "US/Eastern");
		Assertions.assertEquals("2022-04-04T00:00:00-04:00", s);

		d = new Date(1649131199000L);
		s = DateUtils.formatDateInTimezone(d, "US/Eastern");
		Assertions.assertEquals("2022-04-04T23:59:59-04:00", s);
	}

}
